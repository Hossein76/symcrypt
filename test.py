import requests
import unittest
import json

from symcrypt.user.models import UserModel

class TestLogin(unittest.TestCase):
    '''
    Before running this test module please create following user using
    MongoDB console:
        + username: vahid
        + password: test
    '''

    def test_login_success(self):
        r = requests.post(
            'http://localhost:4000/user/login',
            data={
                'username': 'vahid',
                'password': 'test'
            }
        )
        self.assertEqual('', r.json()['err'])

    def test_login_wrong_username(self):
        r = requests.post(
            'http://localhost:4000/user/login',
            data={
                'username': 'majid',
                'password': 'test'
            }
        )
        self.assertEqual(404, r.status_code)

    def test_login_wrong_password(self):
        r = requests.post(
            'http://localhost:4000/user/login',
            data={
                'username': 'vahid',
                'password': 'wrong password'
            }
        )
        self.assertEqual(400, r.status_code)



class TestForgetPassword(unittest.TestCase):
    def test_forget_password_success(self):
        data = {
                'email': 'hossein@gmail.com'
            }
        # data = json.dumps(data)
        r = requests.post(
            'http://127.0.0.1:4000/user/forget_password/',
            json=data,
        )
        self.assertEqual(200, r.status_code)
        
    def test_forget_password_wrong_email(self):
        data = {
                'email': 'aaaa@gmail.com'
            }
        r = requests.post(
            'http://127.0.0.1:4000/user/forget_password/',
            json=data,
        )
        self.assertEqual(404, r.status_code)

class TestChangePassword(unittest.TestCase):
    def test_change_password_success(self):
        user = UserModel.objects(username="hossein").first()
        token = user.token
        data = {
                'token': token,
                'password': 'teeesssst'
            }
        # data = json.dumps(data)
        print("tookennnn", token)
        r = requests.post(
            'http://127.0.0.1:4000/user/change_password/',
            json=data,
        )
        self.assertEqual(200, r.status_code)
        
    def test_change_password_wrong_token(self):
        data = {
                'token': 'aaaaaaa',
                'password': 'hi'
            }
        # data = json.dumps(data)
        r = requests.post(
            'http://127.0.0.1:4000/user/change_password/',
            json=data,
        )
        self.assertEqual(400, r.status_code)

if __name__ == '__main__':
    unittest.main()
