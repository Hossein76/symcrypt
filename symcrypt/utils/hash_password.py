import hashlib


def hash_password(s):
    s = hashlib.md5(s.encode())
    return s.hexdigest()