from mongoengine import Document, EmbeddedDocument, ReferenceField, \
        StringField, DateTimeField, ListField


class UserModel(Document):
    email = StringField(required=True)
    username = StringField()
    firstname = StringField()
    lastname = StringField()
    locations = ListField(StringField())
    date = DateTimeField()
    token = StringField()
    password = StringField()

    def is_active(self):
        return True

    def get_id(self):
        return str(self.id)

    def __unicode__(self):
        return "%s %s" % (self.firstname, self.lastname)
