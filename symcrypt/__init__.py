import importlib
import inspect
import json 

from flask import Flask, session, g, request, jsonify, abort, url_for
from flask_login import LoginManager, login_user
from flask_restful import Api

from mongoengine import connect
from symcrypt.user.models import UserModel
from symcrypt.conf import MONGO_HOST, MONGO_PORT, SECRET_KEY, INSTALLED_MODULES
from .utils import random_generator, hash_password

connect('symcrypt', host=MONGO_HOST, port=MONGO_PORT)


app = Flask(__name__)
app.secret_key = SECRET_KEY

login_manager = LoginManager()
login_manager.init_app(app)

api = Api(app)


@login_manager.user_loader
def load_user(user):
    return UserModel.objects(username=user).first()


@app.route('/user/login', methods=['POST'])
def login():
    u = UserModel.objects(username=request.form['username']).first()
    if u:
        if u.password == hash_password(request.form['password']):
            login_user(u) 
            return jsonify(
                {
                    'err': '',
                }
            ), 200
        else:
            return jsonify(
                {
                    'err': 'username or password is not correct',
                }
            ), 400
    else:
        return jsonify(
            {
                'err': 'the user does not exist'
            }
        ), 404

@app.route('/user/register/', methods=['POST'])
def register():
    data = request.json
    email = data.get('email')
    password = data.get('password')
    username = data.get('username')
    password = hash_password(password)
    user = UserModel(username=username, email=email, password=password)
    user.save()
    return jsonify(
        {
            'msg': 'ok',
        }
    ), 200

@app.route('/user/forget_password/', methods=['POST'])
def forget_password():
    data = request.json
    email = data.get('email')
    user = UserModel.objects(email=email).first()
    if user:
        # TODO: email sender
        user.token = random_generator(20)
        user.save()
        return jsonify(
            {
                'token': user.token,
            }
        ), 200
    else:
        return jsonify(
            {
                'err': 'User does not exist'
            }
        ), 404


@app.route('/user/change_password/', methods=['POST'])
def change_password():
    data = request.json
    token = data.get('token')
    password = data.get('password')
    password = hash_password(password)
    print(password)
    user = UserModel.objects(token=token).first()
    if user:
        user.password = password
        user.token = None
        user.save()

        return jsonify(
            {
                'msg': 'ok',
            }
        ), 200
    else:
        return jsonify(
            {
                'err': 'Invalid token'
            }
        ), 400

for pkg in INSTALLED_MODULES:
    service_script = importlib.import_module(
        "symcrypt.%s.resources" % pkg)
    admin_script = importlib.import_module('symcrypt.%s.models' % pkg)
    for name, obj in inspect.getmembers(service_script):
        if inspect.isclass(obj) and 'symcrypt.%s' % pkg in str(obj) and 'Resource' in str(obj):
            api.add_resource(
                obj,
                '/%s/%s' % (pkg, obj.url),
                endpoint='%s.%s' % (pkg, obj.__name__)
            )
    for name, obj in inspect.getmembers(admin_script):
        if inspect.isclass(obj) and 'api.%s' % pkg in str(obj) and 'Model' in str(obj):
            admin.add_view(ModelView(obj))
