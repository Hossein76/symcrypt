# Database config
MONGO_HOST = 'localhost'
MONGO_PORT = 27017

# API Config
SECRET_KEY = 'Something Secret'

# Modules
INSTALLED_MODULES = [
    'user',
    'crypto',
    # 'price'
]

